FROM python:3.8

WORKDIR /app

COPY code .
RUN pip3 install -r requirements.txt
RUN mkdir results && python3 main.py
EXPOSE 3333
CMD python3 -m http.server 3333