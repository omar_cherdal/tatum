import pandas as pd
import boto3
from bs4 import BeautifulSoup
import requests
import re

S3Bucket = 'demo'
s3 = boto3.resource('s3')

url = "https://github.com/CSSEGISandData/COVID-19/blob/master/archived_data/archived_daily_case_updates"


def get_files_list(url):
    r = requests.get(url)
    html_doc = r.text
    soup = BeautifulSoup(html_doc, features="html.parser")
    a_tags = soup.find_all('a')
    files = ['https://raw.githubusercontent.com' + re.sub('/blob', '', link.get('href'))
            for link in a_tags if '.csv' in link.get('href')]
    return files


def file_reader(filename, separator, country):
    df = pd.read_csv(filename, sep=separator)
    result = df[df['Country/Region'] == country]
    result_file=filename.split("/")[-1].split("?") [0]
    return result
    #result.to_csv("results/"+result_file, index=False)
    # s3.Bucket(S3Bucket).upload_file(filename, filename)


if __name__ == '__main__':
    files = get_files_list(url)
    frame=[]
    for file in files:
        file+= '?raw=true'
        frame.append(file_reader(file, ',', 'US'))
        results = pd.concat(frame, axis=0, ignore_index=True)
        results.to_csv("results/result.csv", index=False)
