# tatum Task



## Pre-requisits

Please make sure to have the following tools installed:
- AWS CLI
- [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started)
## Init Steps
- First deploy the Infra into AWS CLoud
```
aws configure
cd terraform/
terraform init
terraform plan
terraform apply --auto-approve
terraform show
```
## Link Gitlab with k8s Cluster

In Order to allow deployment from Gitlab CI to K8S cluser in our Case (EKS) or on-premise one you need to link them following these steps ## Integrate with your tools

- [Link CLuster](https://docs.gitlab.com/ee/user/project/clusters/add_existing_cluster.html)
## Todos
- Apply security tests on Docker image before pushing it to registery using Trivy or Claire
- Use some gitOps tool like ArgoCd or Flux that keeps monitoring our stack, in our case after executing the helm command we don't know what's happening inside our cluster
- Push terraform State to a dedicated S3 bucket for common usage
- Create a GitFlow and protect the master/main branch
- Maybe it will be better to use S3 a Static content Website hoster 
- Mount S3 as storage to k8S cluster [How](https://efrat19.github.io/blog/mounting-s3-storage-on-eks-pods)
- Continue on EKS implementation
## Issues
- I was out of free tier on AWS so i couldn't test freely 
- Install the runner in my k8S  cluster [Runner Chart](https://charts.gitlab.io)
- TBH I didn't use GitLab CI before so i had to have a quick skim over the Docs to create the basic pipeline
- In the gitlab runner images there is a resolution issue of gitlab site "Could not resolve host: gitlab.com", I tried several tags but kept having the same issue
- 