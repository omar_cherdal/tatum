variable "region" {
  description = "AWS target Region"
  type        = string
  default     = "eu-west-1"
}
variable "bucket_name" {
  description = "Storage Bucket"
  type        = string
}
variable "registry_name" {
  description = "Storage Bucket"
  type        = string
}